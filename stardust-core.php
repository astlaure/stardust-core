<?php
/*
Plugin Name: Stardust Core
Description: Core Plugin for the Stardust Ecosystem. This plugin is required by all other plugins.
Version: 1.0.0
Author Name: Alexandre St-Laurent
Author URI: https://github.com/astlaure
*/

// They have to be here because the stardust-i18n file is required inside a function and the variables are applied to this function instead
$star_language = 'fr';

$star_default_language = 'fr';
$star_supported_languages = array( 'fr', 'en' );
$star_locales = array( 'en' => 'en_CA', 'fr' => 'fr_CA' );
$star_frontpage = array( 'fr' => 'accueil', 'en' => 'homepage' );
$star_custom_posts = array();

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'StardustCore' ) ) {
    class StardustCore {
        function __construct() {
            //
        }

        static public function activate() {
            update_option( 'rewrite_rules', '' );
        }

        static public function deactivate() {
            flush_rewrite_rules();
        }
        
        static public function uninstall() {}
    }
}

if ( class_exists( 'StardustCore' ) ) {
    register_activation_hook( __FILE__, array( 'StardustCore', 'activate' ) );
	register_deactivation_hook( __FILE__, array( 'StardustCore', 'deactivate' ) );
	register_uninstall_hook( __FILE__, array( 'StardustCore', 'uninstall' ) );

    new StardustCore();
}
